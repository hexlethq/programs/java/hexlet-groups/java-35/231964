package exercise;

import java.util.Arrays;

class App {
    // BEGIN
    //Основное задание
    //Первое задание
    public static int getIndexOfMaxNegative(int[] array) {
        for (int i = 0; i < array.length; i++) {
            if ((array[i] == getMaxNegativeValue(array)) && (array[i] < 0)) {
                return i;
            }
        }
        return -1;
    }

    //Второе задание
    public static int[] getElementsLessAverage(int[] array) {
        if (array.length > 0) {
            String result = "";
            for (int i = 0; i < array.length; ++i) {
                result = array[i] <= averageArithmetic(array) ? result + " " + Integer.toString(array[i]) : result;
            }
//            System.out.println(result);
            int[] numArray = Arrays.stream(result.substring(1).split(" "))
                    .map(String::trim).mapToInt(Integer::parseInt).toArray();
            return numArray;
        }
        return array;
    }

    //Метод максимального отрицательного числа
    static int getMaxNegativeValue(int[] array) {
        int max = Integer.MIN_VALUE;
        for (int a : array) {
            max = (a > max) && (a < 0) ? a : max;
        }
        return max;
    }

    //Метод среднего арифметического числа
    public static int averageArithmetic(int[] array) {
        int result = 0;
        for (int sum : array) {
            result = result + sum;
        }
        result = result / array.length;
        return result;
    }

    //Дополнительное Задание
    public static int getSumBeforeMinAndMax(int[] array) {
        int fistNumber;
        int lastNumber;
        if (findValueIndex(array, getMinValue(array)) > findValueIndex(array, getMaxValue(array))) {
            fistNumber = findValueIndex(array, getMaxValue(array));
            lastNumber = findValueIndex(array, getMinValue(array));
        } else {
            fistNumber = findValueIndex(array, getMinValue(array));
            lastNumber = findValueIndex(array, getMaxValue(array));
        }
        int[] newArray = Arrays.copyOfRange(array, fistNumber + 1, lastNumber);
        int sumNewArray = getSumOfArrayValues(newArray);
        return sumNewArray;
    }

    //Метод минимального числа
    static int getMinValue(int[] array) {
        int min = Integer.MAX_VALUE;
        for (int a : array) {
            min = a < min ? a : min;
        }
        return min;
    }

    //Метод Максимального числа
    static int getMaxValue(int[] array) {
        int max = Integer.MIN_VALUE;
        for (int a : array) {
            max = a > max ? a : max;
        }
        return max;
    }

    //Метод ищет индекс значения
    static int findValueIndex(int[] array, int value) {
        for (int i = 0; i < array.length; i++) {
            if (array[i] == value) {
                return i;
            }
        }
        return 0;
    }

    //Метод считает сумму переданного массива
    static int getSumOfArrayValues(int[] array) {
        int sum = 0;
        for (int a : array) {
            sum += a;
        }
        return sum;
    }

    // END
}


