package exercise;

class App {
    // BEGIN
    public static String getTypeOfTriangle(int sideFirst, int sideSecond, int sideThird) {
        if (((sideFirst > 0) && (sideSecond > 0) && (sideThird > 0))
                && ((sideFirst + sideSecond > sideThird) && (sideFirst + sideThird > sideSecond)
                && (sideSecond + sideFirst > sideThird) && (sideSecond + sideThird > sideFirst)
                && (sideThird + sideFirst > sideSecond) && (sideThird + sideSecond > sideFirst))) {
            if ((sideFirst == sideSecond) && (sideFirst == sideThird)) {
                return "Равносторонний";
            } else if (((sideFirst != sideSecond) && (sideFirst != sideThird))
                    && ((sideSecond != sideFirst) && (sideSecond != sideThird))
                    && ((sideThird != sideFirst) && (sideThird != sideSecond))) {
                return "Разносторонний";
            } else if ((sideFirst == sideSecond) || (sideFirst == sideThird)) {
                return "Равнобедренный";
            }
        }
        return "Треугольник не существует";
    }
}


// END


