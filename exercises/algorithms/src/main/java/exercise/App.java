package exercise;

import java.util.Arrays;

class App {
    // BEGIN
    // Основное задание
    // Сортировка пузырьком
    public static int[] sort(int[] array) {
        int count = 0;
        for (int i = 0; i < array.length - 1; i++)
            if (array[i] > array[i + 1]) {
                int temp = array[i];
                array[i] = array[i + 1];
                array[i + 1] = temp;
                count++;
            }
        if (count > 0) {
            sort(array);
        }
        return array;
    }

    //Дополнительное задание
    //Алгоритм сортировки выбором
    public static int[] sortSelection(int[] array) {
        for (int i = 0; i < array.length; i++) {
            int position = i;
            int minValue = array[i];
            for (int k = i + 1; k < array.length; k++) {
                if (array[k] < minValue) {
                    position = k;
                    minValue = array[k];
                }
            }
            array[position] = array[i];
            array[i] = minValue;
        }
        return array;
    }
    // END
}
