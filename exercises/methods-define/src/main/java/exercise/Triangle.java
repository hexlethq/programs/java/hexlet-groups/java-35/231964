package exercise;

class Triangle {
    // BEGIN
    public static double getSquare(int firstSide, int secondSide, int angelBetweenSides) {
        double areaTringle = ((firstSide * secondSide) / 2) * Math.sin((angelBetweenSides * Math.PI) / 180);
        return areaTringle;
    }

    public static void main(String[] args) {
        int firstSide = 4;
        int secondSide = 5;
        int angelBetweenSides = 45;
        double result = Triangle.getSquare(firstSide, secondSide, angelBetweenSides);
        System.out.println(result);

    }

    // END
}


