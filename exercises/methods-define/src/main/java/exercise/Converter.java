package exercise;

class Converter {
    // BEGIN
    public static int convert(int numbers, String outTypes) {
        switch (outTypes) {
            case "b":
                int convertNumbers = (numbers * 1024);
                return convertNumbers;
            case "Kb":
                convertNumbers = (numbers / 1024);
                return convertNumbers;
            default:
                return 0;

        }


    }

    public static void main(Object o) {
        int numbers = 10;
        String intTypes = "Kb";
        String outTypes = "b";
        int result = Converter.convert(numbers, outTypes);
        String endString = (numbers + " " + intTypes + " " + "=" + " " + result + " " + outTypes);
        System.out.println(endString.trim());
    }

    // END
}


