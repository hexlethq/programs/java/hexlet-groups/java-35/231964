package exercise;

class App {
    // BEGIN
    public static String getAbbreviation(String phrase) {
        String result = "";
        phrase = " " + phrase.trim().toUpperCase();
        for (int i = 0; i < phrase.length(); i++) {
            if (((Character.toString(phrase.charAt(i))).equals(" "))
                    && (!(Character.toString(phrase.charAt(i + 1))).equals(" "))) {
                result = result + phrase.charAt(i + 1);
            }
        }
        return result;
    }
    // END
}

