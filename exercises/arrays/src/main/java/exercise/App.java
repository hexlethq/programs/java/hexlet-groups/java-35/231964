package exercise;


class App {
    // BEGIN
    public static int[] reverse(int[] array) {
        int[] arrayClone = array.clone();
        for (int i = 0; i < array.length; ++i) {
            array[i] = arrayClone[(array.length - 1) - i];
        }
        return array;
    }

    public static int mult(int[] array) {
        int result = 1;
        for (int multiplication : array) {
            result = result * multiplication;
        }
        return result;
    }

    // END
}
